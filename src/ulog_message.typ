create or replace type ulog_message as object
(
  log_time timestamp with local time zone,
  severity number,
  msg varchar2(4000)
);