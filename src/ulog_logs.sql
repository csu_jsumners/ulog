create table ulog_logs (
  app raw(32) not null,
  log_time timestamp with local time zone default sysdate not null,
  severity number(1) not null,
  msg varchar2(4000) not null
);

create index ulog_logs_index1 on ulog_logs (
  app
);

create index ulog_logs_index2 on ulog_logs (
  log_time, msg
);

alter table ulog_logs add constraint ulog_logs_fk1 foreign key ( app )
references ulog_apps ( id ) enable;