create or replace
package ulog as
  /**
   * This package provides a set of procedures for logging messages at runtime. It follows
   * the same conventions as the SLF4J logging system. There are some differences, though.
   * Primarily, you have to specify the "application" that is logging whenever you issue a log
   * request. Also, all logs for all applications are written to the same log table -- ulog_logs.
   */

  /**
   * Adds an application to the list of applications that can create log messages. The logging
   * level threshhold will be set to ulog_level.info (INFO).
   *
   * @param in_app_name The name of the application (to be used as a log marker). This case insensitive.
   * @param in_app_desc A description of the application. Merely used for reference by anyone looking at the app list.
   *
   * @return The unique identifier for the application is a RAW value.
   */
  function add_app(in_app_name in varchar2, in_app_desc in varchar2 default null) return raw;
  /**
   * Retrieve the current logging threshold for the specified application.
   *
   * @param in_app_name The application threshold to retrieve
   *
   * @return A logging threshold as a number. Use ulog_level.name_for_level(level) to get a text name for the
   *         log level. If the application is not in the list of logging apps, -1 will be returned.
   */
  function get_app_log_level(in_app_name in varchar2) return number;
  /**
   * Set the logging threshold for the specified application. This can be any value, but it is
   * recommended that you use the levels: ulog_level.trce, ulog_level.debg, ulog_level.info, ulog_level.warn,
   * ulog_level.err, and ulog_level.fatal.
   *
   * @param in_app_name The application who's logging level will be adjusted.
   * @param in_level The new logging level.
   */
  procedure set_app_log_level(in_app_name in varchar2, in_level in number);

  /**
   * Writes a log message to the ulog_logs table. The in_msg parameter can be a formatted string, using the
   * format token `{}`. If the in_fmt_args parameter is not null, then the tokens in the formatted string will
   * be replaced in order. For example, given the following:
   *
   *   in_msg := 'This is {} {}';
   *   in_fmt_args := vargs('an', 'example');
   *
   * Then the resulting logged message would be: 'This is an example'
   *
   * @param in_app A name to identify the application doing the logging. If the application has not already been
   *               registered with `add_app`, it will automatically be created and the logging threshold set to INFO
   *               before the log is processed.
   * @param in_level The severity of the message. If the applications current logging threshold is less than this
   *                 value then the log will not be recorded.
   * @param in_msg The message to be recorded. This can be a format string as described above. The maximum length of
   *               the message is 4,000 characters. If the format substitutions will cause the message to exceed this
   *               limitation, the format string is written with as many substitutions as possible.
   * @param in_fmt_args The values to substitute into the format string.
   */
  procedure log(in_app in varchar2, in_level in number, in_msg in varchar2, in_fmt_args in vargs default null);
  /**
   * A wrapper for `log` with the severity level set to ulog_level.trce.
   */
  procedure trce(in_app in varchar2, in_msg in varchar2, in_fmt_args in vargs default null);
  /**
   * A wrapper for `log` with the severity level set to ulog_level.debg.
   */
  procedure debg(in_app in varchar2, in_msg in varchar2, in_fmt_args in vargs default null);
  /**
   * A wrapper for `log` with the severity level set to ulog_level.info.
   */
  procedure info(in_app in varchar2, in_msg in varchar2, in_fmt_args in vargs default null);
  /**
   * A wrapper for `log` with the severity level set to ulog_level.warn.
   */
  procedure warn(in_app in varchar2, in_msg in varchar2, in_fmt_args in vargs default null);
  /**
   * A wrapper for `log` with the severity level set to ulog_level.err.
   */
  procedure err(in_app in varchar2, in_msg in varchar2, in_fmt_args in vargs default null);
  /**
   * A wrapper for `log` with the severity level set to ulog_level.fatal.
   */
  procedure fatal(in_app in varchar2, in_msg in varchar2, in_fmt_args in vargs default null);

  /**
   * Retrieve the log messages for a given application. This allows you to easily query for your application's
   * log messages like so:
   *
   *   select * from table(ulog.get_logs('foo')) where severity = ulog_level.warn();
   *
   * Where 'foo' is the applcation's logs to retrieve.
   *
   * @param in_app_name The application who's logs you want to view.
   */
  function get_logs(in_app_name in varchar2) return ulog_messages;

end ulog;