create table ulog_apps (
  id raw(32) default sys_guid() not null,
  app_name varchar2(128) not null,
  app_desc varchar2(2048),
  log_level number(1) default 3 not null
);

create unique index ulog_apps_index1 on ulog_apps (
  id
);

create unique index ulog_apps_index2 on ulog_apps (
  app_name asc
);

alter table ulog_apps add constraint ulog_apps_pk primary key ( id ) using
  index ulog_apps_index1 enable;

comment on table ulog_apps is 'List of applications targetable by utl_log';
comment on column ulog_apps.app_name is 'The name of the application';
comment on column ulog_apps.app_desc is 'A short description of the application';