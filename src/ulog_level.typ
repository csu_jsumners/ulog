create or replace type ulog_level as object
(
  deflt number, -- info
  static function trce return number,
  static function debg return number,
  static function info return number,
  static function warn return number,
  static function err return number,
  static function fatal return number,
  static function name_for_level(lvl in number) return varchar2,
  constructor function ulog_level return self as result
);

create or replace
type body ulog_level as

  static function name_for_level(lvl in number) return varchar2 as
    rslt varchar2(5);
  begin
    case lvl
      when ulog_level.trce then rslt := 'trace';
      when ulog_level.debg then rslt := 'debug';
      when ulog_level.info then rslt := 'info';
      when ulog_level.warn then rslt := 'warn';
      when ulog_level.err then rslt := 'error';
      when ulog_level.fatal then rslt := 'fatal';
      else rslt := 'unknown';
    end case;

    return rslt;
  end name_for_level;

  static function trce return number as
  begin
    return 1;
  end trce;


  static function debg return number as
  begin
    return 2;
  end debg;

  static function info return number as
  begin
    return 3;
  end info;

  static function warn return number as
  begin
    return 4;
  end warn;

  static function err return number as
  begin
    return 5;
  end err;

  static function fatal return number as
  begin
    return 6;
  end fatal;

  constructor function ulog_level return self as result as
  begin
    self.deflt := ulog_level.info;
    return;
  end ulog_level;

end;