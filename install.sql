prompt

/*
11g
alter session set plsql_optimize_level = 3;
alter session set plsql_code_type = 'NATIVE';
*/
alter session set plsql_optimize_level = 2;

-- TODO: add logic for modifying an existing installation
@@uninstall.sql

prompt --------------------------------;
prompt -- Compiling objects for ULOG --;
prompt --------------------------------;
-- The order is important
@@src/vargs.typ
@@src/ulog_message.typ
@@src/ulog_messages.typ
@@src/ulog_level.typ
@@src/ulog_apps.sql
@@src/ulog_logs.sql
@@src/ulog.spec
@@src/ulog.body