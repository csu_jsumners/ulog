# ULOG

ULOG is a simple logging system for Oracle stored procedures. With ULOG, you can
log messages in your stored procedures according to their importance. The levels
supported are:

* trace
* debug
* info
* warn
* error
* fatal

ULOG is designed to be as unobtrusive and quick as possible. It should not
introduce significant overhead into your procedure executions.

## Example Usage

Let's assume you have a package named "foo" that you'd like to internally track
with logging. Then it would be as simple as:

```plsql
-- some code prior to this
ulog.info('foo', 'my_var1 = {}; my_var2 = {}', vargs(my_var1, my_var2));
-- some more code
-- ...
-- ...
ulog.debg('foo', 'Terrible things are happening!');
-- ...
```

There are a couple things to note in this example:

1. Each log includes the name of the package, or "application", as the first
   parameter. This is because ULOG is designed to work in a schema that has
   multiple "applications" stored in it
2. The second parameter can include a format token, `{}`, or not. If a format
   token is included, then a `vargs` object must be passed as the third
   parameter. The `vargs` object type is included with ULOG
3. The `debg` (debug) log will only be written to the database if the "foo"
   application is configured to include that level of logging

Once you have logged some messages for an application, retrieving those messages
is as simple as:

```plsql
select * from table(ulog.get_logs('foo'));
```

## Documentation

### Types

#### ULOG_LEVEL
The ULOG_LEVEL type defines aliases for the numeric logging level codes. In any
situation where you would need to supply a "severity" level, you can use any
of the following methods on this type:

* `ulog_level.default()`: returns the default logging level as defined by ULOG (INFO)
* `ulog_level.trce()`: TRACE - 1
* `ulog_level.debug()`: DEBUG - 2
* `ulog_level.info()`: INFO - 3
* `ulog_level.warn()`: WARN - 4
* `ulog_level.err()`: ERROR - 5
* `ulog_level.fatal()`: FATAL - 6

This type also includes a method for converting a level code to its textual
name: `ulog_level.name_for_level(lvl)`. Simply pass in a known level code,
e.g. 1, and it will return the name for that code ('trace' in this example).

#### VARGS
The VARGS type provides a simple way to supply an indetermite number of format
substitution values to the logging methods. It is merely a table of `varchar2`
values.

#### ULOG_MESSAGE
This type is used by `ulog.get_logs(app)` to represent a log message. It has
no other use.

#### ULOG_MESSAGES
This type is used by `ulog.get_logs(app)` to represent a list of log messages.
It has no other use.

### Tables

#### ULOG_APPS
The ULOG_APPS table tracks the applications using the ULOG system and their
respective logging level thresholds. You can view this table to see which
applications are using ULOG, a possible description of those applications, and
what severity level messages will be recorded.

#### ULOG_LOGS
The ULOG_LOGS table is used to record all log messages written by all applications
using the ULOG system. For each log message recorded by an application, the
application's identifier (as is recorded in ULOG_APPS), the message's time of
record, severity, and message will be written to this table.

**Note:** it is recommended you periodically clear or trim this table. The ULOG
system does not do this for you. It is left up to the user (DBA) to determine how
many log messages they wish to keep.

### Package
The only package included with ULOG is the ULOG package. This package provides
the primary interface for ULOG, as is described in this section.

Full documentation for the package is provided in the packages's spec. This
section merely provides a brief overview.

#### Managing Applications
There are three methods provided by the ULOG package that allow you to manage
the applications that use ULOG. These methods are:

* `add_app(app_name, app_desc)`: used to manually add an application to the ULOG system
* `get_app_log_level(app_name)`: used to get the current logging level code for an application
* `set_app_log_level(app_name, severity)`: used to adjust the logging level threshold for an application

#### Reading Logs
The ULOG package provides a utility method for reading an application's log
messages (instead of directly querying ULOG_LOGS): `get_logs(app_name)`. To use
this method, simply issue a query using this method in a pipelined table function
in the FROM clause.\

Example:

    select * from table(ulog.get_logs('foo'));

#### Writing Log Messages
The main method for writing log messages is `ulog.log`. This method requires
three parameters and accepts an optional fourth. The required parameters are:

1. `in_app`: the name of the application writing the log
2. `in_level`: the severity of the log message (see ULOG_LEVEL above)
3. `in_msg`: the log message to record

The optional fourth parameter is `in_fmt_args` and is an instance of `VARGS`.
If this parameter is passed in, then the `in_msg` will be parsed for format
tokens in the form `{}`. These will be replaced in order with the values in
`in_fmt_args` from first to last (provided the message length will not
exceed 4000 characters).

When this method is invoked, it first retrieves the application id from ULOG_APPS.
If an id doesn't exist for the named application, it creates a record in
ULOG_APPS for the named application and sets the logging level to INFO. It then
check the passed in severity level, `in_level`, against the configured level
for the named application. If the `in_level` is greater than or equal to the
configured level, the log message is parsed and written. Otherwise, the method
simply returns without doing any further work.

As a convenience, there are wrapper methods for each default logging level.
These methods supply the severity level for you and are named thusly:

* `ulog.trce`
* `ulog.debg`
* `ulog.info`
* `ulog.warn`
* `ulog.err`
* `ulog.fatal`

# License
[http://jsumners.mit-license.org](http://jsumners.mit-license.org)

THE MIT LICENSE (MIT) Copyright © 2014 James Sumners james.sumners@clayton.edu
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.