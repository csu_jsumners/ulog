drop package ulog;
drop table ulog_logs;
drop table ulog_apps;
drop type ulog_level;
drop type ulog_messages;
drop type ulog_message;
drop type vargs;